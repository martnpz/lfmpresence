Discord rich presence for last.fm written in Go.

+ Usage:
``` 
git clone https://gitlab.com/martnpz/lfmpresence.git
go get
go build -o lfmpresence
lfmpresence [username] 
 ```
