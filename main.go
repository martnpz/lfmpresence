package main

import (
    "fmt"
    "os"
    "time"

    "github.com/ndyakov/go-lastfm"
    "github.com/hugolgst/rich-go/client"
)

const (
    discordAppKey  = "868813214740271135"
    lfmApiKey      = "baf88353bfb937e0d7fba4f6316f860f"
)

func richPresence(user string) {
    // Change status after 30 seconds.
    for true {
        fmt.Println("Loading...")

        // Load Discord app.
        err := client.Login(discordAppKey)
        if err != nil {
            panic(err)
        }

        // Get Recent Tracks from User.
        lfm := lastfm.New(lfmApiKey, "")
        response, err := lfm.User.GetRecentTracks(user, 0, 0, 0, 0)
        if err != nil {
            fmt.Println("Error:")
            fmt.Println(err)
            return
        }

        track     := response.RecentTracks[0]

        // Display rich presence.
        var cover string
        if track.Image[0].URL != "" {
            cover = track.Image[0].URL
        } else {
            cover = "https://www.last.fm/static/images/lastfm_avatar_twitter.52a5d69a85ac.png"
        }

        if track.NowPlaying != "" {
            client.SetActivity(client.Activity{
                Details:    "🎵 "  + track.Name,
                State:      "👤 " + track.Artist.Name,
                LargeImage: cover,
                SmallImage: "https://i.gifer.com/7gQj.gif",
                Buttons: []*client.Button{
                    &client.Button{
                        Label: "Free CP",
                        Url:   "https://www.last.fm/user/" + user,
                    },
                },
            })

            if err != nil {
                panic(err)
            }
        }

        // Discord will only show the presence if the app is running
        // Sleep for a few seconds to see the update
        time.Sleep(time.Second * 30)
    }
}

func help() {
	fmt.Println("Usage:")
	fmt.Println("lfmpresence [username]")
}

func main()  {
    if len(os.Args) < 2 {
        help()
    } else {
        richPresence(os.Args[1])
    }
}

